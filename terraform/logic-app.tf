module "logic_app" {
  source          = "https://gitlab.com/bupa-demo/az-logic-app-complete.git"

# Resource group 
  resource_group_name         = var.resource_group_name
  location                    = var.location

# App service plan
  app_service_plan_name        = var.app_service_plan_name  
  os_type                      = var.os_type  
  sku_name                     = var.sku_name  
  worker_count                 = var.sku_name == "Y1" ? null : var.worker_count  
  maximum_elastic_worker_count = var.maximum_elastic_worker_count  
  app_service_environment_id   = var.app_service_environment_id  
  per_site_scaling_enabled     = var.per_site_scaling_enabled  

# Logic App
  storage_name                        = var.storage_name
  replication_type                    = var.replication_type
  account_kind                        = var.account_kind
  account_tier                        = var.account_tier
  access_tier                         = var.access_tier
  enable_large_file_share             = var.enable_large_file_share
  enable_hns                          = var.enable_hns
  enable_https_traffic_only           = var.enable_https_traffic_only
  min_tls_version                     = var.min_tls_version
  allow_nested_items_to_be_public     = var.allow_nested_items_to_be_public
  access_list                         = var.access_list
  service_endpoints                   = var.service_endpoints
  traffic_bypass                      = var.traffic_bypass
  blob_delete_retention_days          = var.blob_delete_retention_days
  blob_cors                           = var.blob_cors
  encryption_scopes                   = var.encryption_scopes
  infrastructure_encryption_enabled   = var.infrastructure_encryption_enabled
  nfsv3_enabled                       = var.nfsv3_enabled
  default_network_rule                = var.default_network_rule
  shared_access_key_enabled           = var.shared_access_key_enabled
  blob_versioning_enabled             = var.blob_versioning_enabled
  container_delete_retention_days     = var.container_delete_retention_days

#PE
  private_endpoint_name               = var.private_endpoint_name
  subresource_names                   = var.subresource_names
  is_manual_connection                = var.is_manual_connection
  private_dns_zone                    = var.private_dns_zone
  private_endpoint_name_vnl           = var.private_endpoint_name_vnl

#vNET
  vnet_name             = var.vnet_name
  vnet_cidr             = var.vnet_cidr
  dns_servers           = var.dns_servers

#Route
  logic_app_route_table_name    = var.logic_app_route_table_name
  disable_bgp_route_propagation = var.disable_bgp_route_propagation
  logic_app_route_name          = var.logic_app_route_name
  enable_force_tunneling        = var.enable_force_tunneling

# NSG
  logic_app_network_security_group_name   = var.logic_app_network_security_group_name
  ingress_rules                           = var.ingress_rules
  egress_rules                            = var.egress_rules

# Subnet
  logic_app_subnet_name               = var.logic_app_subnet_name
  logic_app_subnet_cidr_list          = var.logic_app_subnet_cidr_list

  default_tags                        = var.default_tags
}
